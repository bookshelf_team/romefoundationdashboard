﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GAREportDashboard.ApiClient
{
    public class CMECollection
    {
        public int total_count { get; set; }
        public List<Entry> entries { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Entry
    {
        public string id { get; set; }
        public string form_id { get; set; }
        public object post_id { get; set; }
        public string date_created { get; set; }
        public string date_updated { get; set; }
        public string is_starred { get; set; }
        public string is_read { get; set; }
        public string ip { get; set; }
        public string source_url { get; set; }
        public string user_agent { get; set; }
        public string currency { get; set; }
        public object payment_status { get; set; }
        public object payment_date { get; set; }
        public object payment_amount { get; set; }
        public object payment_method { get; set; }
        public object transaction_id { get; set; }
        public object is_fulfilled { get; set; }
        public object created_by { get; set; }
        public object transaction_type { get; set; }
        public string status { get; set; }

        [JsonProperty("1.3")]
        public string _13 { get; set; }

        [JsonProperty("1.6")]
        public string _16 { get; set; }
        public string _2 { get; set; }
        public string _3 { get; set; }

        [JsonProperty("4.6")]
        public string _46 { get; set; }
        public string _5 { get; set; }
        public string _7 { get; set; }
        public string _8 { get; set; }

        [JsonProperty("9.1")]
        public string _91 { get; set; }

        [JsonProperty("9.3")]
        public string _93 { get; set; }

        [JsonProperty("9.4")]
        public string _94 { get; set; }

        [JsonProperty("9.5")]
        public string _95 { get; set; }

        [JsonProperty("9.6")]
        public string _96 { get; set; }
        public string _10 { get; set; }
        public string _131 { get; set; } // repeated above appended 1a t last
        public string _14 { get; set; }
        public string _15 { get; set; }

        [JsonProperty("1.2")]
        public string _12 { get; set; }

        [JsonProperty("1.4")]
        public string _141 { get; set; }  // repeated above appended 1a t last

        [JsonProperty("1.8")]
        public string _18 { get; set; }

        [JsonProperty("4.1")]
        public string _41 { get; set; }

        [JsonProperty("4.2")]
        public string _42 { get; set; }

        [JsonProperty("4.3")]
        public string _43 { get; set; }

        [JsonProperty("4.4")]
        public string _44 { get; set; }

        [JsonProperty("4.5")]
        public string _45 { get; set; }
        public string _6 { get; set; }

        [JsonProperty("9.2")]
        public string _92 { get; set; }
    }

   


}