﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GAREportDashboard.ApiClient
{
    public class UserListResponse
    {
        public List<ResisteredUser> UserList { get;set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class AvatarUrls
    {
        public string _24 { get; set; }
        public string _48 { get; set; }
        public string _96 { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Collection
    {
        public string href { get; set; }
    }

    public class Links
    {
        public List<Self> self { get; set; }
        public List<Collection> collection { get; set; }
    }

    public class ResisteredUser
    {
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string slug { get; set; }
        public AvatarUrls avatar_urls { get; set; }
        public List<object> meta { get; set; }
        public string yoast_head { get; set; }
        public Links _links { get; set; }
    }


}