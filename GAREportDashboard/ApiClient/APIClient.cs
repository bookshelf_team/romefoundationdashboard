﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace GAREportDashboard.ApiClient
{
    public class APIClient
    {

        #region private variables        
        private static Uri tokenEndpoint = null;
        private static TokenResponse tokenResponse = null;

        public static TokenResponse TokenResponse
        {
            get
            {

                if (tokenResponse == null)
                    tokenResponse = GetOAuthAccessToken();
                return tokenResponse;
            }


        }

        #endregion

        #region Constructor

        static APIClient()
        {
            //  //log.Debug("------------[START] reading messaging  related cofig -------------");
            tokenEndpoint = new Uri("https://theromefoundation.org/wp-json/jwt-auth/v1/token?password=%@2%e%23klTdxqiVTx1LCTd)wY&username=apiuser");// new Uri(ConfigurationManager.AppSettings["smsTokenEndPoint"]);
                                                                                                                                                    //  //log.Debug("------------[END] reading messaging  related cofig ---------------");
        }

        #endregion

        //   #region public methods
        /// <summary>
        /// Get Access Token. Call this fuction on signin function.
        /// </summary>
        /// <returns>IAuthorizationState, Contains Access/Refesh Token details</returns>
        public static TokenResponse GetOAuthAccessToken()// (SMSConfig callerConfig)
        {
            try
            {
                //  //log.Debug("------------checking for Caller SMS Setting  ---------------");
                //  if (callerConfig != null)
                //  {
                //log.Debug("------------start calling to " + tokenEndpoint + " to get token ---------------");

                //scope format should be like this
                // "scope=cn+mail+sn+givenname+uid+employeeNumber&grant_type=password&username=" + callerConfig.HarmonyId + "&password=" + callerConfig.Harmonypassword;
                //  string scope = callerConfig.Scope;

                // //log.Debug("-----------ClientCredential for " + callerConfig.smsClientId + ":" + callerConfig.smsClientSecretKey + " is  " + callerConfig.encryptedClientSecretKey);

                var client = new RestClient(tokenEndpoint);
                var request = new RestRequest(Method.POST);
                // request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("authorization", "Basic " + callerConfig.encryptedClientSecretKey);
                // request.AddParameter("application/x-www-form-urlencoded", scope, ParameterType.RequestBody);
                // request.AddHeader("Accept", "*/*");
                IRestResponse<TokenResponse> response = client.Execute<TokenResponse>(request);

                //log.Debug("End token response");

                return response.Data;
                // }
                // else
                //  {
                // log.Trace("Authorize caller config not found");
                //return null;
                //  }

            }
            catch (Exception ex)
            {
                //log.Debug("------------Exception at getAuthAccessToken Method ---------------");
                return null;
            }
        }


        public static UserListResponse GetUserList()
        {
            var client = new RestClient(new Uri("https://theromefoundation.org/wp-json/wp/v2/users"));
            var request = new RestRequest(Method.GET);
            // request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + TokenResponse.token);
            // request.AddParameter("application/x-www-form-urlencoded", scope, ParameterType.RequestBody);
            // request.AddHeader("Accept", "*/*");
            IRestResponse<List<ResisteredUser>> response = client.Execute<List<ResisteredUser>>(request);
            UserListResponse userList = new UserListResponse();
            userList.UserList = response.Data;
            return userList;

        }

        public static CMECollection GetCMEList()
        {
            var client = new RestClient(new Uri("https://theromefoundation.org/wp-json/gf/v2/forms/20/entries"));
            var request = new RestRequest(Method.GET);
            // request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + TokenResponse.token);
            // request.AddParameter("application/x-www-form-urlencoded", scope, ParameterType.RequestBody);
            // request.AddHeader("Accept", "*/*");
            IRestResponse<CMECollection> response = client.Execute<CMECollection>(request);
            return response.Data;
            //UserListResponse userList = new UserListResponse();
           // userList.UserList = response.Data;
           // return userList;

        }
        //public static ICMApiResponse SendSMS(SMSRequest userAttribute, SMSConfig currentCallerConfig, string vendorName)
        //{

        //    //log.Debug("------------SMS Request from  " + currentCallerConfig.AuthorizeCaller + "With MessageName " + userAttribute.SMSType);
        //    //log.Debug("------------start  checking  " + currentCallerConfig.AuthorizeCaller + " SMS Config ---------------");
        //    ICMApiResponse response = new ICMApiResponse();
        //    ICMSmsRequest smsRequest = new ICMSmsRequest();
        //    smsRequest.vendorName = "Hyperpointe";//vendorName;
        //    smsRequest.eventType = currentCallerConfig.MessageId;
        //    smsRequest.sms = new Sms[userAttribute.recipients.Length];
        //    int smsRecipientcount = 0;
        //    try
        //    {
        //        foreach (Novo.Entities.Recipient recipient in userAttribute.recipients)
        //        {
        //            string phoneNumber = Regex.Replace(recipient.smsNumber, @"[^\d]", "");
        //            recipient.smsNumber = phoneNumber;
        //            if (recipient.smsNumber.Length == 10)
        //            {
        //                recipient.smsNumber = "1" + phoneNumber; // prefix 1  if mobile no not contains country code of USA
        //            }
        //            //if ((recipient.attributes == null || recipient.attributes.Length == 0) && (userAttribute.defaultAttributes != null))
        //            //{
        //            //    recipient.attributes = userAttribute.defaultAttributes;

        //            //}
        //            smsRequest.sms[smsRecipientcount] = new Sms();
        //            smsRequest.sms[smsRecipientcount].externalId = Convert.ToString(userAttribute.ExternalId);
        //            smsRequest.sms[smsRecipientcount].firstName = userAttribute.defaultAttributes[0].attributeValue;
        //            smsRequest.sms[smsRecipientcount].lastName = userAttribute.defaultAttributes[0].attributeValue;
        //            smsRequest.sms[smsRecipientcount].phoneNumber = userAttribute.recipients[0].smsNumber;
        //            smsRecipientcount++;
        //        }

        //        //log.Debug("Createing SMSRequest Object");

        //        //EpsilonSMSRequest smsRequest = new EpsilonSMSRequest(); // creating new object otherwise json serialiser will serialize child class properites as well
        //        //smsRequest.defaultAttributes = userAttribute.defaultAttributes;
        //        //smsRequest.recipients = userAttribute.recipients;
        //        ////log.Debug("--- SMSRequest Object as Josn object-- ");
        //        ////log.Debug(Newtonsoft.Json.JsonConvert.SerializeObject(smsRequest));
        //        //ICSMessagingAccessToken token = GetOAuthAccessToken(currentCallerConfig);
        //        //if (token != null)
        //        //{
        //        //    //log.Debug("Token recieved: " + token.AccessToken);
        //        var client = new RestClient(currentCallerConfig.SMSAPI);
        //        var request = new RestRequest(Method.POST);
        //        request.AddHeader("Authorization", "Basic " + currentCallerConfig.encryptedClientSecretKey);
        //        // request.AddHeader("X-OUID", currentCallerConfig.XOUID);
        //        request.AddHeader("content-type", "application/json");
        //        request.AddJsonBody(smsRequest);
        //        //  request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
        //        //  request.AddParameter("application/json", requestData, ParameterType.RequestBody);
        //        log.Trace("[Start] calling ICM sms method");
        //        IRestResponse<ICMActualApiResponse> smsresponse = client.Execute<ICMActualApiResponse>(request);
        //        log.Trace("[End] calling ICM sms method end");
        //        response.IcsActualResponse = smsresponse.Data;
        //        response.RequestString = Newtonsoft.Json.JsonConvert.SerializeObject(smsRequest);
        //        response.ResponseString = smsresponse.Content;
        //        response.status = Convert.ToString(smsresponse.StatusCode);
        //        //log.Debug("--Epsilon SMS Message Response--");
        //        //log.Debug(Newtonsoft.Json.JsonConvert.SerializeObject(response));
        //    }
        //    catch (Exception ex)
        //    {

        //        //log.Debug("Exception occurred while sending SMS");
        //        throw ex;
        //    }
        //    return response;
        //}

        //public static ICMApiResponse OptOutHealthConciergeSMSProgram(SMSConfig currentCallerConfigs, string mobileNumber)
        //{
        //    ICMApiResponse epsilonSMSResponse = new ICMApiResponse();
        //    try
        //    {
        //        if (currentCallerConfigs != null && (!string.IsNullOrEmpty(mobileNumber)))
        //        {
        //            string smsNumber = mobileNumber;
        //            string PhNumber = Regex.Replace(smsNumber, @"[^\d]", "");

        //            if (PhNumber.Length == 10)
        //            {
        //                PhNumber = "1" + PhNumber; // prefix 1  if mobile no not contains country code of USA
        //            }

        //            log.Info("HealthConciergeSMS configuration  found start calling out out API.");
        //            string MoUrl = currentCallerConfigs.MOUrl.Replace("MobileNumber", PhNumber);

        //            var client = new RestClient(HttpUtility.UrlDecode(MoUrl));
        //            var request = new RestRequest(Method.POST);
        //            // request.AddHeader("postman-token", "73bd6d2b-b1cb-3825-458b-888941c6fe10");
        //            request.AddHeader("cache-control", "no-cache");
        //            request.AddHeader("authorization", "Basic " + currentCallerConfigs.encryptedMOClientSecretKey);
        //            IRestResponse response = client.Execute(request);
        //            epsilonSMSResponse.status = response.StatusCode.ToString();
        //            epsilonSMSResponse.ResponseString = response.Content;
        //            epsilonSMSResponse.RequestString = HttpUtility.UrlDecode(MoUrl);


        //        }
        //        else
        //        {
        //            log.Info("HealthConciergeSMS configuration not found or may Mobile or phone is empty.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info("Exception occured while calling OptOutHealthConciergeSMSProgram Method in EpsilonMessagingclient.");
        //        epsilonSMSResponse.status = "FAILED";
        //        epsilonSMSResponse.ResponseString = ex.Message;
        //    }
        //    return epsilonSMSResponse;

        //}

    }

}