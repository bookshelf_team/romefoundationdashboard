﻿using GAREportDashboard.ApiClient;
using GAREportDashboard.Filters;
using GAREportDashboard.Models;
using Google.Apis.Analytics.v3;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace GAREportDashboard.Controllers
{
    [CustomAuthorize]
    public class AnalyticsController : Controller
    {

        private static string viewId = ConfigurationManager.AppSettings["ViewId"].ToString();
        private static string ServiceAccountCredentialsFileName = ConfigurationManager.AppSettings["ServiceAccountCredentialFileName"].ToString();

        public static AnalyticsReportingService GetService(string apiKey)
        {
            try
            {
                if (string.IsNullOrEmpty(apiKey))
                    throw new ArgumentNullException("api Key");

                return new AnalyticsReportingService(new BaseClientService.Initializer()
                {
                    ApiKey = "AIzaSyASEGadq72GJFZD4g-Aa_TNlpcuoSUF_Yo",
                    ApplicationName = "API key 1",
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create new AnalyticsReporting Service", ex);
            }
        }

        public AnalyticsController()
        {

        }
        // GET: Analytics
        public ActionResult Index()
        {


            //return View();
            return RedirectToAction("OverView");
        }
        public ActionResult OverView(AnalyticsViewModel model)
        {
            var pageViewmodel = new AnalyticsViewModel();
            if (model != null)
                pageViewmodel = model;


            if (ModelState.IsValid)
            {
                if (model.Startdate >= DateTime.Today)
                {
                    ModelState.AddModelError("Start Date", "StartDate can not be future date");
                    return View(model);
                }

                //string DateString = model.Startdate.ToShortDateString();
                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);

                string Startdate = string.Format("{0:u}", model.Startdate);
                string EndDate = string.Format("{0:u}", model.Enddate);



                string directoryName = System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);


                #region ServiceAccountEnabled
                var credential = Google.Apis.Auth.OAuth2.GoogleCredential.FromFile(Path.Combine(directoryName, ServiceAccountCredentialsFileName).Replace("file:\\", ""))
                    .CreateScoped(new[] { Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService.Scope.AnalyticsReadonly });
                #endregion


                //        string[] scopes = new string[] {
                //AnalyticsService.Scope.Analytics };               // view and manage your Google Analytics data


                //        var clientId = "785887950713-tlh035i6i295rehv2g81o7skpmdd540p.apps.googleusercontent.com";      // From https://console.developers.google.com
                //        var clientSecret = "-AU8AU-o9t-ecrzcH93aSxtE";          // From https://console.developers.google.com
                //                                           // here is where we Request the user to give us access, or use the Refresh Token that was previously stored in %AppData%
                //        var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets
                //        {
                //            ClientId = clientId,
                //            ClientSecret = clientSecret
                //        },
                //          scopes,
                //          Environment.UserName,
                //          CancellationToken.None,
                //          new FileDataStore("Daimto.GoogleAnalytics.Auth.Store")).Result;




                using (var analytics = new Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService(new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                }))
                {
                    var request = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {
                        new ReportRequest{
                            DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:date" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:sessions", Alias = "Sessions"} },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId =viewId// "240644632"
                        },
                         new ReportRequest{
                            DateRanges =  new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:pagePath" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:pageviews", Alias = "pageviews" } },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId =viewId// "240644632"
                        },
                          new ReportRequest{
                            DateRanges =  new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:referralPath" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:organicSearches", Alias = "Activeusers" } },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId = viewId// "240644632"
                        }
                    }
                    });
                    //new Dimension { Name = "ga:pagePath" }
                    //new Metric{ Expression = "ga:users", Alias = "Users"},





                    var response = request.Execute();

                    #region UserSessionAnalyticsData


                    var analyticsData = response.Reports[0].Data;
                    if (response.Reports[0].Data.Rows != null)
                    {
                        model.AnalyticsRecords = analyticsData.Rows.ToList();
                        model.ColumnHeader = response.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> amodel = new List<AnalyticalDataModel>();

                    DataTable dt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        dt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        dt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    dt.Rows.Add(result, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = dt.Rows[i][0].ToString();
                        cls.ColumnValue = int.Parse(Convert.ToString(dt.Rows[i][1]));
                        amodel.Add(cls);
                    }




                    ViewBag.result = amodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion

                    #region UserPageViewAnalyticsData


                    var pvanalyticsData = response.Reports[1].Data;
                    if (response.Reports[1].Data.Rows != null)
                    {
                        pageViewmodel.AnalyticsRecords = pvanalyticsData.Rows.ToList();
                        pageViewmodel.ColumnHeader = response.Reports[1].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        pageViewmodel.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> pvmodel = new List<AnalyticalDataModel>();

                    DataTable pageViewdt = new DataTable();

                    foreach (var r in pageViewmodel.ColumnHeader.Dimensions)
                    {
                        pageViewdt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in pageViewmodel.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        pageViewdt.Columns.Add(r.Name);

                    }

                    foreach (var r in pageViewmodel.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {

                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    pageViewdt.Rows.Add(d, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < pageViewdt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = pageViewdt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(pageViewdt.Rows[i][1]));
                        pvmodel.Add(cls);
                    }
                    ViewBag.pageViewModel = pvmodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList().OrderByDescending(c => c.Metric);
                    #endregion

                    #region ActivePages


                    var activePagerequest = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {

                          new ReportRequest{
                            DateRanges =  new[] { new DateRange { StartDate = "Today", EndDate = "Today" } },
                            Dimensions = new[] { new Dimension{ Name = "ga:pagePath" }},//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:users", Alias = "ActiveUsers" } },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId = "240644632"
                        }
                    }
                    });
                    var activePagesResult = activePagerequest.Execute();

                    var ActivePageanalyticsData = activePagesResult.Reports[0].Data;
                    if (activePagesResult.Reports[0].Data.Rows != null)
                    {
                        pageViewmodel.AnalyticsRecords = ActivePageanalyticsData.Rows.ToList();
                        pageViewmodel.ColumnHeader = activePagesResult.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        pageViewmodel.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> activepagevmodel = new List<AnalyticalDataModel>();

                    DataTable activepageViewdt = new DataTable();

                    foreach (var r in pageViewmodel.ColumnHeader.Dimensions)
                    {
                        activepageViewdt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in pageViewmodel.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        activepageViewdt.Columns.Add(r.Name);

                    }

                    foreach (var r in pageViewmodel.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {

                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    activepageViewdt.Rows.Add(d, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < activepageViewdt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = activepageViewdt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(activepageViewdt.Rows[i][1]));
                        activepagevmodel.Add(cls);
                    }
                    ViewBag.ActivepageViewModel = JsonConvert.SerializeObject(activepagevmodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList().OrderByDescending(c => c.Metric));
                    #endregion

                    #region TopRefferal
                    var TopRefferalPagerequest = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {

                          new ReportRequest{
                           DateRanges = new[] { new DateRange { StartDate = "Today", EndDate = "Today" } },
                            Dimensions = new[] { new Dimension{ Name = "ga:fullReferrer" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:users", Alias = "ActiveUsers" } },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId = viewId//"240644632"
                        }
                    }
                    });
                    var TopRefferalPagerequestResult = TopRefferalPagerequest.Execute();

                    var TopRefferalPageanalyticsData = TopRefferalPagerequestResult.Reports[0].Data;
                    if (TopRefferalPagerequestResult.Reports[0].Data.Rows != null)
                    {
                        pageViewmodel.AnalyticsRecords = TopRefferalPageanalyticsData.Rows.ToList();
                        pageViewmodel.ColumnHeader = TopRefferalPagerequestResult.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        pageViewmodel.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> Refferalactivepagevmodel = new List<AnalyticalDataModel>();

                    DataTable referalactivepageViewdt = new DataTable();

                    foreach (var r in pageViewmodel.ColumnHeader.Dimensions)
                    {
                        referalactivepageViewdt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in pageViewmodel.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        referalactivepageViewdt.Columns.Add(r.Name);

                    }

                    foreach (var r in pageViewmodel.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {

                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    referalactivepageViewdt.Rows.Add(d, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < referalactivepageViewdt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = referalactivepageViewdt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(referalactivepageViewdt.Rows[i][1]));
                        Refferalactivepagevmodel.Add(cls);
                    }
                    ViewBag.ReferalpageViewModel = JsonConvert.SerializeObject(Refferalactivepagevmodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList().OrderByDescending(c => c.Metric));
                    #endregion




                    #region TopLocation
                    var TopLocationPagerequest = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {

                          new ReportRequest{
                           DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:country" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:users", Alias = "ActiveUsers" } },//, new Metric { Expression = "ga:pageViews" }},
                            ViewId = viewId// "240644632"
                        }
                    }
                    });
                    var TopLocationPagerequestResult = TopLocationPagerequest.Execute();

                    var TopLocationPageanalyticsData = TopLocationPagerequestResult.Reports[0].Data;
                    if (TopLocationPagerequestResult.Reports[0].Data.Rows != null)
                    {
                        pageViewmodel.AnalyticsRecords = TopLocationPageanalyticsData.Rows.ToList();
                        pageViewmodel.ColumnHeader = TopLocationPagerequestResult.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        pageViewmodel.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> Locationactivepagevmodel = new List<AnalyticalDataModel>();

                    DataTable LocationactivepageViewdt = new DataTable();

                    foreach (var r in pageViewmodel.ColumnHeader.Dimensions)
                    {
                        LocationactivepageViewdt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in pageViewmodel.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        LocationactivepageViewdt.Columns.Add(r.Name);

                    }

                    foreach (var r in pageViewmodel.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {

                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    LocationactivepageViewdt.Rows.Add(d, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < LocationactivepageViewdt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = LocationactivepageViewdt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(LocationactivepageViewdt.Rows[i][1]));
                        Locationactivepagevmodel.Add(cls);
                    }
                    ViewBag.LocationpageViewModel = Locationactivepagevmodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList().OrderByDescending(c => c.Metric);
                    #endregion

                    #region DeviceCategoryAnalyticsData


                    var Devicerequest = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {
                            new ReportRequest {
                                DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                                Dimensions = new[] { new Dimension { Name = "ga:deviceCategory" } },//,new Dimension { Name = "ga:country" }},
                                                                                                    // Metrics = new[] { new Metric{ Expression = "ga:deviceCategory", Alias = "Device Type" } },// ,new Metric { Expression = "ga:sessions" } },
                                ViewId =viewId //"240644632"
                            }
                        }

                    });
                    //new Dimension { Name = "ga:pagePath" }
                    //new Metric{ Expression = "ga:users", Alias = "Users"},





                    var DeviceCategoryresponse = Devicerequest.Execute();




                    var DeviceCategoryanalyticsData = DeviceCategoryresponse.Reports[0].Data;
                    if (DeviceCategoryresponse.Reports[0].Data.Rows != null)
                    {
                        model.AnalyticsRecords = DeviceCategoryanalyticsData.Rows.ToList();
                        model.ColumnHeader = DeviceCategoryresponse.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> DeviceCategorymodel = new List<AnalyticalDataModel>();

                    DataTable Devicedt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        Devicedt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        Devicedt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            // string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    Devicedt.Rows.Add(d, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < Devicedt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = Devicedt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(dt.Rows[i][1]));
                        DeviceCategorymodel.Add(cls);
                    }




                    ViewBag.DeviceCategory = DeviceCategorymodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion

                }
            }

            return View(pageViewmodel);
        }

        public ActionResult BounceRate(AnalyticsViewModel model)
        {

            var pageViewmodel = new AnalyticsViewModel();
            if (model != null)
                pageViewmodel = model;


            if (ModelState.IsValid)
            {
                if (model.Startdate >= DateTime.Today)
                {
                    ModelState.AddModelError("Start Date", "StartDate can not be future date");
                    return View(model);
                }

                //string DateString = model.Startdate.ToShortDateString();
                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);

                string Startdate = string.Format("{0:u}", model.Startdate);
                string EndDate = string.Format("{0:u}", model.Enddate);



                string directoryName = System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                var credential = Google.Apis.Auth.OAuth2.GoogleCredential.FromFile(Path.Combine(directoryName, "serviceAccount.json").Replace("file:\\", ""))
                    .CreateScoped(new[] { Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService.Scope.AnalyticsReadonly });

                using (var analytics = new Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService(new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                }))
                {
                    var request = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {
                        new ReportRequest{
                            DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:date" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:bounceRate", Alias = "bounceRate" } },// ,new Metric { Expression = "ga:sessions" } },
                            ViewId =viewId// "240644632"
                        },
                         new ReportRequest{
                            DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:deviceCategory" } },//,new Dimension { Name = "ga:country" }},
                           Metrics = new[] { new Metric{ Expression = "ga:bounceRate", Alias = "bounceRate" } },// ,new Metric { Expression = "ga:sessions" } },
                            ViewId =viewId// "240644632"
                        }
                    }
                    });
                    //new Dimension { Name = "ga:pagePath" }
                    //new Metric{ Expression = "ga:users", Alias = "Users"},





                    var response = request.Execute();

                    #region BounceRateAnalyticsData


                    var analyticsData = response.Reports[0].Data;
                    if (response.Reports[0].Data.Rows != null)
                    {
                        model.AnalyticsRecords = analyticsData.Rows.ToList();
                        model.ColumnHeader = response.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> amodel = new List<AnalyticalDataModel>();

                    DataTable dt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        dt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        dt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    dt.Rows.Add(result, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = dt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(dt.Rows[i][1]));
                        amodel.Add(cls);
                    }




                    ViewBag.BounceRate = amodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion

                    #region BounceRateByDeviceTypeAnalyticsData


                    var DeviceBounceRateData = response.Reports[1].Data;
                    if (response.Reports[1].Data.Rows != null)
                    {
                        model.AnalyticsRecords = DeviceBounceRateData.Rows.ToList();
                        model.ColumnHeader = response.Reports[1].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> DeviceTypeBounceRatemodel = new List<AnalyticalDataModel>();

                    DataTable DeviceTypeBouncedt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        DeviceTypeBouncedt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        DeviceTypeBouncedt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            //string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    DeviceTypeBouncedt.Rows.Add(d, v);



                                }
                            }
                        }
                    }
                    DeviceTypeBouncedt.Columns.Add("Device");
                    DeviceTypeBouncedt.Columns.Add("BounceRate");
                    DeviceTypeBouncedt.Rows.Add("Mobile", 5);
                    DeviceTypeBouncedt.Rows.Add("Tablet", 10);
                    DeviceTypeBouncedt.Rows.Add("All Device", 15);


                    for (int i = 0; i < DeviceTypeBouncedt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = DeviceTypeBouncedt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(DeviceTypeBouncedt.Rows[i][1]));
                        DeviceTypeBounceRatemodel.Add(cls);
                    }




                    ViewBag.DeviceTypeBounceRate = DeviceTypeBounceRatemodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion

                }

            }
            return View();
        }

        public ActionResult TopDevices(AnalyticsViewModel model)
        {

            var pageViewmodel = new AnalyticsViewModel();
            if (model != null)
                pageViewmodel = model;


            if (ModelState.IsValid)
            {
                if (model.Startdate >= DateTime.Today)
                {
                    ModelState.AddModelError("Start Date", "StartDate can not be future date");
                    return View(model);
                }

                //string DateString = model.Startdate.ToShortDateString();
                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);

                string Startdate = string.Format("{0:u}", model.Startdate);
                string EndDate = string.Format("{0:u}", model.Enddate);



                string directoryName = System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                var credential = Google.Apis.Auth.OAuth2.GoogleCredential.FromFile(Path.Combine(directoryName, "serviceAccount.json").Replace("file:\\", ""))
                    .CreateScoped(new[] { Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService.Scope.AnalyticsReadonly });

                #region DeviceCategoryAnalyticsData

                using (var analytics = new Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService(new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                }))
                {
                    var request = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {
                        new ReportRequest{
                            DateRanges = new[] { new DateRange { StartDate = model.Startdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), EndDate = model.Enddate.AddDays(1).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) } },
                            Dimensions = new[] { new Dimension{ Name = "ga:deviceCategory" } },//,new Dimension { Name = "ga:country" }},
                          // Metrics = new[] { new Metric{ Expression = "ga:deviceCategory", Alias = "Device Type" } },// ,new Metric { Expression = "ga:sessions" } },
                            ViewId =viewId// "240644632"
                        }
                    }
                    });
                    //new Dimension { Name = "ga:pagePath" }
                    //new Metric{ Expression = "ga:users", Alias = "Users"},





                    var DeviceCategoryresponse = request.Execute();




                    var DeviceCategoryanalyticsData = DeviceCategoryresponse.Reports[0].Data;
                    if (DeviceCategoryresponse.Reports[0].Data.Rows != null)
                    {
                        model.AnalyticsRecords = DeviceCategoryanalyticsData.Rows.ToList();
                        model.ColumnHeader = DeviceCategoryresponse.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> DeviceCategorymodel = new List<AnalyticalDataModel>();

                    DataTable dt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        dt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        dt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    dt.Rows.Add(result, v);

                                }
                            }
                        }
                    }




                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = dt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(dt.Rows[i][1]));
                        DeviceCategorymodel.Add(cls);
                    }




                    ViewBag.DeviceCategory = DeviceCategorymodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion
                }

            }
            return View();
        }

        public ActionResult Video(AnalyticsViewModel model)
        {

            var pageViewmodel = new AnalyticsViewModel();
            if (model != null)
                pageViewmodel = model;


            if (ModelState.IsValid)
            {
                if (model.Startdate >= DateTime.Today)
                {
                    ModelState.AddModelError("Start Date", "StartDate can not be future date");
                    return View(model);
                }

                //string DateString = model.Startdate.ToShortDateString();
                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);

                string Startdate = string.Format("{0:u}", model.Startdate);
                string EndDate = string.Format("{0:u}", model.Enddate);



                string directoryName = System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                var credential = Google.Apis.Auth.OAuth2.GoogleCredential.FromFile(Path.Combine(directoryName, "serviceAccount.json").Replace("file:\\", ""))
                    .CreateScoped(new[] { Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService.Scope.AnalyticsReadonly });

                #region DeviceCategoryAnalyticsData

                using (var analytics = new Google.Apis.AnalyticsReporting.v4.AnalyticsReportingService(new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                }))
                {
                    var request = analytics.Reports.BatchGet(new GetReportsRequest
                    {
                        ReportRequests = new[] {
                        new ReportRequest{
                            DateRanges = new[] { new DateRange { StartDate = "Today", EndDate = "Today" } },
                            Dimensions = new[] { new Dimension{ Name = "ga:eventCategory" },new Dimension{ Name = "ga:eventAction" },new Dimension{Name = "ga:eventLabel" } },//,new Dimension { Name = "ga:country" }},
                          // Metrics = new[] { new Metric{ Expression = "ga:totalEvents", Alias = "Events" } },// ,new Metric { Expression = "ga:sessions" } },
                            ViewId =viewId// "240644632"
                        }
                    }
                    });
                    //new Dimension { Name = "ga:pagePath" }
                    //new Metric{ Expression = "ga:users", Alias = "Users"},





                    var DeviceCategoryresponse = request.Execute();




                    var DeviceCategoryanalyticsData = DeviceCategoryresponse.Reports[0].Data;
                    if (DeviceCategoryresponse.Reports[0].Data.Rows != null)
                    {
                        model.AnalyticsRecords = DeviceCategoryanalyticsData.Rows.ToList();
                        model.ColumnHeader = DeviceCategoryresponse.Reports[0].ColumnHeader;
                    }
                    else // No data was send from Google Analytics API, so pass an empty list to the client.
                    {
                        model.ColumnHeader = DeviceCategoryresponse.Reports[0].ColumnHeader;
                        model.AnalyticsRecords = new List<ReportRow>();
                    }
                    List<AnalyticalDataModel> DeviceCategorymodel = new List<AnalyticalDataModel>();

                    DataTable dt = new DataTable();

                    foreach (var r in model.ColumnHeader.Dimensions)
                    {
                        dt.Columns.Add(r.Replace("ga:", ""));


                    }
                    foreach (var r in model.ColumnHeader.MetricHeader.MetricHeaderEntries)
                    {
                        dt.Columns.Add(r.Name);

                    }

                    foreach (var r in model.AnalyticsRecords)
                    {
                        foreach (var d in r.Dimensions)
                        {
                            string result = DateTime.ParseExact(d, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                            foreach (var m in r.Metrics)
                            {
                                foreach (var v in m.Values)
                                {
                                    dt.Rows.Add(result, v);

                                }
                            }
                        }
                    }

                    dt.Rows.Add(1, 1, 1, 1);


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AnalyticalDataModel cls = new AnalyticalDataModel();
                        cls.columnName = dt.Rows[i][0].ToString();
                        cls.ColumnValue = double.Parse(Convert.ToString(dt.Rows[i][1]));
                        DeviceCategorymodel.Add(cls);
                    }




                    ViewBag.EventCategory = DeviceCategorymodel.Select(m => new { Dimension = m.columnName, Metric = m.ColumnValue }).ToList();
                    #endregion
                }

            }
            return View();
        }

        public ActionResult Users(AnalyticsViewModel model)
        {
           
            UserListResponse userlist = APIClient.GetUserList();

            return View(userlist);
        }

        public ActionResult UserList(AnalyticsViewModel model)
        {

            UserListResponse userlist = APIClient.GetUserList();

            return View(userlist.UserList);
        }

        public ActionResult CMECollection(AnalyticsViewModel model)
        {
            
          CMECollection cMECollection= APIClient.GetCMEList();

            return View(cMECollection.entries);
        }

    }


}