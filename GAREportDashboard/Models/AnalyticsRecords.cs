﻿using Google.Apis.AnalyticsReporting.v4.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAREportDashboard.Models
{
    public class AnalyticsViewModel

    {
        private DateTime startdate = DateTime.MinValue; // field
        private DateTime endDate = DateTime.MinValue; // field

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Startdate
        {
            get
            {
                if (startdate == DateTime.MinValue)
                    return startdate = DateTime.Now.AddDays(-7);
                else
                    return startdate;
            }
            set
            {
                startdate = value;
            }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Enddate
        {
            get
            {
                if (endDate == DateTime.MinValue)
                    return endDate = DateTime.Now;
                else
                    return endDate;
            }
            set
            {
                if (value != DateTime.Today && value != DateTime.MinValue)
                    endDate = value;
            }
        }
        public  ColumnHeader ColumnHeader { get; set; }
        public List<ReportRow> AnalyticsRecords { get; set; }

    }

    public class AnalyticalDataModel
    {
        public string columnName { get; set; }
        public double ColumnValue { get; set; }
    }
}